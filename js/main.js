// ----1----
const paragraphs = document.querySelectorAll('p')
console.log(paragraphs);

for (p of paragraphs) {
  p.style.backgroundColor = "#ff0000"
}

// ----2----
const optionsList = document.getElementById('optionsList');
console.log("optionsList:", optionsList);

const parentOptionsList = optionsList.parentElement;
console.log("parent of OptionsList :", parentOptionsList);


if (optionsList.hasChildNodes()) {
  const childOptionsList = optionsList.childNodes;
  console.log("children of OptionsList:", childOptionsList);

  for (ch of childOptionsList) {
    console.log(ch.nodeName, ch.nodeType);
  }
}

// ----3----

const testParagraph = document.querySelector("#testParagraph");
testParagraph.innerText = "This is a paragraph";
console.log(testParagraph);

// ----4----

const listOfMainHeader = document.querySelector('.main-header').children;
console.log("list of main-header:", listOfMainHeader);

for(elem of listOfMainHeader) {
  elem.classList.add('nav-item')
}

// ----5----

const sectionTitle = document.querySelectorAll(".section-title");

for (elem of sectionTitle) {
  elem.classList.remove('section-title');
}

console.log('removed section-title class:', sectionTitle);
